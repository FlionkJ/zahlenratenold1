tool
extends EditorPlugin


func _enter_tree():
    add_custom_type("LineEditRx", "LineEdit", preload("line_edit_rx.gd"), preload("icon.svg"))

func _exit_tree():
    remove_custom_type("LineEditRx")
