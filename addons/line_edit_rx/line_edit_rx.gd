tool
extends LineEdit

export var validation: String = ".*" setget set_validation, get_validation
export var block_invalid: bool = false

signal text_changed_rx(new_text, is_valid) # _on_text_changed_rx(new_text: String, is_valid: bool)

var _validation_rx = RegEx.new()
var _old_text: String
var _is_input_valid: bool setget ,is_input_valid

func _ready():
    #self.validation = validation
    self._old_text = self.text
    self.connect("text_changed", self, "_on_text_changed")

func set_validation(new_validation: String):
    validation = new_validation
    if OK != self._validation_rx.compile(new_validation):
        push_error("invalid regex: " + new_validation)
        self._validation_rx = RegEx.new()
        validation = ""

func get_validation() -> String:
    return validation

func is_input_valid() -> bool:
    return _is_input_valid

func _on_text_changed(new_text: String):
    var rx_result = self._validation_rx.search(new_text)
    self._is_input_valid = rx_result != null && rx_result.get_string() == new_text
    if not self._is_input_valid && self.block_invalid:
        self.text = self._old_text
        set_cursor_position(self.text.length())
    else:
        self._old_text = new_text
    emit_signal("text_changed_rx", new_text, self._is_input_valid)
