extends Node

static func check(input_number: int, tries: int):
    if input_number == g_data.result:
        return [tries + 1, "", true] 
    elif input_number > g_data.result:
        return [tries + 1, "smaller", false]
    else:
        return [tries + 1, "higher", false]

static func delete_old_player_result():
    g_data.player_results = []        

static func start():
    randomize()
    g_data.result = randi()%(g_data.highest_number + 1)
