extends Control

onready var credits_text = get_node("margin_container/scroll_container/v_box_container/changelog_text")

func _ready():
    var file = File.new()
    file.open("res://data/language/changelog_" + g_settings.language() + ".txt", File.READ)
    var text = file.get_as_text()
    file.close()
    credits_text.set_text(text)

func _on_back_button_pressed():
    get_tree().change_scene("res://scene/menu/settings.tscn")
