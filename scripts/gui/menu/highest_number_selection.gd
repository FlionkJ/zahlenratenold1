extends Control

var game = preload("res://scripts/game/base_game.gd")

onready var user_input = get_node("margin_container/center_container/v_box_container/user_input_highest_number")
onready var user_output = get_node("margin_container/user_output/user_output")
onready var user_output_timer = get_node("margin_container/user_output/user_output_timer")

func _on_user_input_highest_number_text_entered(new_text: String):
    if new_text > "0":
        g_data.highest_number = int(new_text)
        game.start()
        game.delete_old_player_result()
        get_tree().change_scene("res://scene/game/main_game_scene.tscn")
    elif new_text == "0":
        user_input.set_text("")
        user_output.set_text(tr("Highest_Number_Warning"))
        user_output_timer.start()
    else:
        user_output.set_text(tr("Highest_Number_Warning_Null"))
        user_output_timer.start()

func _on_backbutton_pressed():
    get_tree().change_scene("res://scene/menu/selection.tscn")

func _on_user_output_timer_timeout():
    user_output.set_text("")
