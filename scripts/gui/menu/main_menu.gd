extends Node

func _on_start_button_pressed():
    get_tree().change_scene("res://scene/menu/selection.tscn")

func _on_settings_button_pressed():
    get_tree().change_scene("res://scene/menu/settings.tscn")

func _on_quit_button_pressed():
    g_settings.save()
    get_tree().quit()
