extends Node

func _on_tutorial_button_pressed():
    pass

func _on_1_player_button_pressed():
    g_data.max_player = 0
    get_tree().change_scene("res://scene/menu/highest_number_selection.tscn")

func _on_2_player_button_pressed():
    g_data.max_player = 1
    get_tree().change_scene("res://scene/menu/highest_number_selection.tscn")

func _on__player_button_pressed():
    get_tree().change_scene("res://scene/menu/questenmark_player_selection.tscn")

func _on_back_button_pressed():
    get_tree().change_scene("res://scene/menu/main_menu.tscn")
