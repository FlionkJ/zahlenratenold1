extends Control

onready var language_option_button = get_node("center_container/language_option_button")
onready var user_output_text = get_node("margin_container/user_information/user_output")
onready var user_output_timer = get_node("margin_container/user_information/user_output_timer")

var language_new

func _ready():
    self.language_new = g_settings.language()
    var popup = language_option_button.get_popup()
    popup.clear()
    for lang in g_settings.AVAILABLE_LANG:
        popup.add_item(tr(lang))
    popup.connect("id_pressed", self, "_menuID")
    language_option_button.set_text(tr(g_settings.language()))

func _on_cancel_button_pressed():
    get_tree().change_scene("res://scene/menu/main_menu.tscn")

func _on_language_option_button_item_selected(idx):
    self.language_new = g_settings.AVAILABLE_LANG[idx]

func _on_save_button_pressed():
    g_settings.update_lang(self.language_new)

    user_output_text.set_text(tr("User_Output_Save"))
    user_output_timer.start()
    self._ready()
    
func _on_user_output_timer_timeout():
    user_output_text.set_text("")

func _on_credits_button_pressed():
    get_tree().change_scene("res://scene/menu/credits.tscn")

func _on_changelog_button_pressed():
    get_tree().change_scene("res://scene/menu/changelog.tscn")
