extends MarginContainer

func _on_main_menu_button_pressed():
    get_tree().change_scene("res://scene/menu/main_menu.tscn")

func _on_restart_button_pressed():
    var game = preload("res://scripts/game/base_game.gd")
    game.start()
    get_tree().change_scene("res://scene/game/main_game_scene.tscn")

func _on_selection_menu_button_pressed():
    get_tree().change_scene("res://scene/menu/selection.tscn")
