extends Control

onready var warning_text = get_node("margin_container/center_container/warning")
onready var user_input = get_node("center_container/v_box_container/userinput")
onready var useroutput_timer = get_node("margin_container/center_container/user_output_timer")

func _on_userinput_text_entered(new_text):
    if new_text > "0":
        g_data.max_player = int(new_text) - 1
        get_tree().change_scene("res://scene/game/main_game_scene.tscn")
    elif new_text == "0":
        warning_text.set_text(tr("Highest_Number_Warning"))
        user_input.set_text("")
    else:
        warning_text.set_text(tr("Highest_Number_Warning_Null"))

func _on_user_output_timer_timeout():
    warning_text.set_text("")

func _on_back_button_pressed():
    get_tree().change_scene("res://scene/menu/selection.tscn")
