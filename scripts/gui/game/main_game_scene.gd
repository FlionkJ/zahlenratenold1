extends Node

var game = preload("res://scripts/game/base_game.gd")
var player_name_scene = load("res://scene/game/player_name.tscn")
var next_player_scene = load("res://scene/game/next_player.tscn")

onready var player_name_path = get_node("panel_container/scroll_container/v_box_container")
onready var user_input = get_node("margin_container/v_box_container/margin_container/user_input")
onready var user_info = get_node("margin_container/v_box_container/user_info")
onready var user_tries = get_node("margin_container/v_box_container/margin_container2/h_box_container/tries_output")

var tries: int = 0

func _ready():
    for i in range(g_data.max_player + 1):
        var node = self.player_name_scene.instance()
        node.set_name(tr("Player") + str(i + 1))
        self.player_name_path.add_child(node)
    g_data.current_player = 0
    self.player_name_path.get_child(g_data.current_player).set("custom_colors/font_color", Color("#16c00c"))

func _on_user_input_text_entered(new_text):
    if user_input.is_editable() == true:   
        var check: Array = self.game.check(int(new_text), tries)
        self.tries = check[0]
        if check[2] == true:
            g_data.player_results.append(self.tries)
            self.player_name_path.get_child(g_data.current_player).set("custom_colors/font_color", Color("#e01111"))
            g_data.current_player = g_data.current_player + 1
            self.tries = 0
            if g_data.current_player > g_data.max_player:
                get_tree().change_scene("res://scene/game/win.tscn")
            else:
                self.player_name_path.get_child(g_data.current_player).set("custom_colors/font_color", Color("#16c00c"))
                self.user_input.set_text("")
                self.user_input.set_editable(false)
                self.add_child(self.next_player_scene.instance())
                self.user_tries.set_text("")
        self.user_info.set_text(tr(check[1]))
        self.user_tries.set_text(str(check[0]))
