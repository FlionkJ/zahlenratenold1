extends Control

onready var player_results_path = get_node("margin_container/scroll_container/grid_container")

func _ready():
    for i in range(g_data.max_player + 1):
        var player_name_node = Label.new()
        player_name_node.add_font_override("font",load("res://resources/noto_sans_regular.tres"))
        player_name_node.set_text(tr("Player") + str(i + 1) + ":")
        player_results_path.add_child(player_name_node)
        var player_tries_node = Label.new()
        player_tries_node.add_font_override("font",load("res://resources/noto_sans_regular.tres"))
        player_tries_node.set_text(str(g_data.player_results[i]))
        player_results_path.add_child(player_tries_node)

func _on_continue_button_pressed():
    get_tree().change_scene("res://scene/menu/selection_after_game.tscn")
