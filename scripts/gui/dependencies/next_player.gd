extends Control

onready var user_info_path = get_node("margin_container/center_container/user_info")

func _ready():
    user_info_path.set_text(tr("Player") + str(g_data.current_player + 1) + " " + (tr("Next_Player_Info")))

func _on_continue_button_pressed():
    self.get_parent().get_node("margin_container/v_box_container/margin_container/user_input").set_editable(true)
    self.get_parent().remove_child(self)
